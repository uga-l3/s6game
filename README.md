# s6game

### Work team

* **AI** : Benoît SONZOGNI, Antoine CLAVEL (branch ai)
* **Engine/Network** : Noé MASSE (branch engine)
* **GUI** : Thomas HERVÉ (branch gui)

__**Mise en place de l'environement de travail (Pour le projet principal)**__


__Etape 1 : Cloner le dépôt distant__
```
git clone git@gitlab.com:uga-l3/s6game.git
cd s6game
git checkout <engine|gui|ia>
```

__Etape 2 : Générer le projet__
Si vous utilisez **Eclipse** :
```
./gradlew eclipse
```
Si vous utilisez **IntelliJ IDEA** :
```
./gradlew idea
```

__Etape 3 : Importer le projet__
```Import > General > Existing project into workspace```

__Etape 4 : Changer de branche__
```
git checkout <engine|gui|ia>
```